DROP VIEW IF EXISTS vNonCouverts;
DROP VIEW IF EXISTS vAlerteMeteo;
DROP VIEW IF EXISTS vHistoriqueCapteur;
DROP VIEW IF EXISTS vLieuxConcernes;
DROP TABLE IF EXISTS POSSEDE_PREVISION;
DROP TABLE IF EXISTS BULLETIN;
DROP TABLE IF EXISTS PREVISION_SIMPLE;
DROP TABLE IF EXISTS VENT;
DROP TABLE IF EXISTS PRECIPITATION;
DROP TABLE IF EXISTS TEMPERATURE;
DROP TABLE IF EXISTS HISTORIQUE_CAPTEUR;
DROP TABLE IF EXISTS CAPTEUR;
DROP TABLE IF EXISTS RESPONSABLE;
DROP TABLE IF EXISTS PERSONNE;
DROP TABLE IF EXISTS LIEU;
DROP TABLE IF EXISTS DEPARTEMENT;
DROP TABLE IF EXISTS REGION;

CREATE TABLE REGION(
  nom VARCHAR(30) PRIMARY KEY,
  outre_mer BOOLEAN
);
CREATE TABLE DEPARTEMENT(
  numero INT PRIMARY KEY,
  nom VARCHAR(30) NOT NULL,
  region VARCHAR(30) NOT NULL,
  FOREIGN KEY (region) REFERENCES REGION(nom)
);

CREATE TABLE LIEU(
 nom VARCHAR(30) PRIMARY KEY,
 altitude INT,
 climat VARCHAR(20),
 departement INT NOT NULL,
 departement2 INT,
 type CHAR(1),
 FOREIGN KEY (departement) REFERENCES DEPARTEMENT(numero),
 FOREIGN KEY (departement2) REFERENCES DEPARTEMENT(numero),
 CONSTRAINT CHK_Type CHECK ((type='v' AND departement2 IS NULL) OR (type='m'))
);

CREATE TABLE PERSONNE(
 prenom VARCHAR (20) NOT NULL,
 nom VARCHAR(20) NOT NULL,
 email VARCHAR(40) NULL,
 tel VARCHAR(10) UNIQUE,
 PRIMARY KEY (prenom,nom)
);

CREATE TABLE RESPONSABLE(
 prenom_responsable VARCHAR (20),
 nom_responsable VARCHAR(20),
 lieu VARCHAR(30),
 PRIMARY KEY (prenom_responsable,nom_responsable,lieu),
 FOREIGN KEY (prenom_responsable,nom_responsable) REFERENCES PERSONNE(prenom,nom),
 FOREIGN KEY (lieu) REFERENCES LIEU(nom)
);

CREATE TABLE CAPTEUR(
 reference VARCHAR (20) PRIMARY KEY,
 hs BOOLEAN,
 acquisition JSON NOT NULL
);

CREATE TABLE HISTORIQUE_CAPTEUR(
 capteur VARCHAR(20),
 lieu VARCHAR(30),
 date_debut DATE NOT NULL,
 date_fin DATE,
 PRIMARY KEY (capteur,lieu),
 FOREIGN KEY (capteur) REFERENCES CAPTEUR(reference),
 FOREIGN KEY (lieu) REFERENCES LIEU(nom),
 CONSTRAINT CHK_time CHECK (date_fin >= date_debut)
);

CREATE TABLE TEMPERATURE(
 id INT PRIMARY KEY,
 capteur VARCHAR(20) NOT NULL,
 description VARCHAR(150),
 temp_reel INT,
 temp_ressentie INT,
 FOREIGN KEY (capteur) REFERENCES CAPTEUR(reference)
);

CREATE TABLE PRECIPITATION(
 id INT PRIMARY KEY,
 capteur VARCHAR(20) NOT NULL,
 description VARCHAR(150),
 type VARCHAR(10) NOT NULL,
 FOREIGN KEY (capteur) REFERENCES CAPTEUR(reference)
);

CREATE TABLE VENT(
 id INT PRIMARY KEY,
 capteur VARCHAR(20) NOT NULL,
 description VARCHAR(150),
 force_vent INT NOT NULL,
 direction CHAR(1),
 force_rafale INT,
 FOREIGN KEY (capteur) REFERENCES CAPTEUR(reference),
 CONSTRAINT CHK_direction CHECK (direction='S' OR direction='N' OR direction='O' OR direction='E')
);

CREATE TABLE PREVISION_SIMPLE(
 id INT PRIMARY KEY,
 valeur INT,
 unite_mesure VARCHAR(20),
 capteur VARCHAR(20) NOT NULL,
 description VARCHAR(150),
 FOREIGN KEY (capteur) REFERENCES CAPTEUR(reference)
);

CREATE TABLE BULLETIN(
 date_prevision DATE,
 periode VARCHAR(20),
 lieu VARCHAR(30),
 id_temperature INT,
 id_precipitation INT,
 id_vent INT,
 alertes JSON,
 PRIMARY KEY(date_prevision,periode),
 FOREIGN KEY (id_temperature) REFERENCES TEMPERATURE(id),
 FOREIGN KEY (id_precipitation) REFERENCES PRECIPITATION(id),
 FOREIGN KEY (id_vent) REFERENCES VENT(id),
 CONSTRAINT CHK_periode CHECK (periode='matin' OR periode='après-midi' OR periode='soirée' OR periode='nuit')
);
  
CREATE TABLE POSSEDE_PREVISION(
 prevision_simple INT,
 date_bulletin DATE,
 periode_bulletin VARCHAR(20),
 PRIMARY KEY (prevision_simple,date_bulletin,periode_bulletin),
 FOREIGN KEY (date_bulletin,periode_bulletin) REFERENCES BULLETIN(date_prevision,periode),
 FOREIGN KEY (prevision_simple) REFERENCES PREVISION_SIMPLE(id)
);
    
INSERT INTO REGION VALUES('Rhône-Alpes', false);
INSERT INTO REGION VALUES('Hauts-De-France',false);

INSERT INTO DEPARTEMENT VALUES (38,'Isere','Rhône-Alpes');
INSERT INTO DEPARTEMENT VALUES (26,'Drôme','Rhône-Alpes');
INSERT INTO DEPARTEMENT VALUES (60,'Oise','Hauts-De-France');

INSERT INTO LIEU VALUES ('COMPIEGNE',100,NULL,60,NULL,'v');
INSERT INTO LIEU VALUES ('VERCORS',2341,'montagnard',38,26,'m');
INSERT INTO LIEU VALUES ('GRENOBLE',1000,'montagnard',38,NULL,'v');
INSERT INTO LIEU VALUES ('ANJOU',700,'montagnard',38,NULL,'v');

INSERT INTO PERSONNE VALUES ('Amaury', 'Walckiers', 'amaury.walckiers@etu.utc.fr', '0600112233');
INSERT INTO PERSONNE VALUES ('Jinane', 'Ben Salem', 'jinane.ben-salem@etu.utc.fr', '0612345678');
INSERT INTO RESPONSABLE VALUES ('Jinane','Ben Salem','COMPIEGNE');
INSERT INTO RESPONSABLE VALUES ('Amaury','Walckiers','VERCORS');
INSERT INTO CAPTEUR VALUES ('14564800',false,
                         '{"fournisseur":"Picarro","type":"achat","pays":"USA","ville":"Santa Clara","code_postal":95054,"numero":3105,"rue":"Patrick Henry Dr.","prix":1000,"date_ac":"2011-11-24" }');
INSERT INTO CAPTEUR VALUES ('78564600',false,
                         '{"fournisseur":"Picarro","type":"location","pays":"USA","ville":"Santa Clara","code_postal":95054,"numero":3105,"rue":"Patrick Henry Dr.","prix":45,"date_ac":"2015-08-10" }');
INSERT INTO CAPTEUR VALUES ('78564700',false,
                         '{"fournisseur":"Picarro","type":"location","pays":"USA","ville":"Santa Clara","code_postal":95054,"numero":3105,"rue":"Patrick Henry Dr.","prix":30,"date_ac":"2015-08-10" }');
INSERT INTO CAPTEUR VALUES ('54720400',false,
                         '{"fournisseur":"UTC","type":"possession","pays":"France","ville":"Compiègne","code_postal":60200,"numero":-1,"adresse":"Roger Couttolenc","prix":0,"date_ac":"2010-04-12" }');
INSERT INTO CAPTEUR VALUES ('54720300',false,
                         '{"fournisseur":"UTC","type":"possession","pays":"France","ville":"Compiègne","code_postal":60200,"numero":-1,"adresse":"Roger Couttolenc","prix":0,"date_ac":"2010-04-12" }');
INSERT INTO HISTORIQUE_CAPTEUR VALUES('14564800','VERCORS','2012-04-20','2020-03-15');
INSERT INTO HISTORIQUE_CAPTEUR VALUES('14564800','ANJOU','2020-04-16',NULL);
INSERT INTO HISTORIQUE_CAPTEUR VALUES('78564600','VERCORS','2020-03-15',NULL);
INSERT INTO HISTORIQUE_CAPTEUR VALUES('78564700','VERCORS','2020-03-15',NULL);
INSERT INTO HISTORIQUE_CAPTEUR VALUES('54720400','COMPIEGNE','2017-06-08',NULL);
INSERT INTO HISTORIQUE_CAPTEUR VALUES('54720300','COMPIEGNE','2017-06-08',NULL);
  
INSERT INTO TEMPERATURE VALUES (1,'78564700','Forte température','23','27');
INSERT INTO TEMPERATURE VALUES (2,'78564700','Forte température','30','32');
INSERT INTO VENT VALUES (1,'78564600','Bourrasques',120,'N',140);
INSERT INTO VENT VALUES (2,'78564600','',60,'S',70);
INSERT INTO PREVISION_SIMPLE VALUES(1,'10','%','78564700','Humidite');
  
INSERT INTO PRECIPITATION VALUES (1,'54720400','','neige');
INSERT INTO PREVISION_SIMPLE VALUES(2,'70','%','54720400','Humidite');
  
INSERT INTO BULLETIN VALUES('2020-03-25','soirée','VERCORS',1,NULL,1,'[
{"type":"Tempete","vigilance":"rouge","seuil":100,"restrictions":"Acces interdit à la montagne"},
{"type":"Forte Chaleur","vigilance":"orange","seuil":22,"restrictions":""}]');
INSERT INTO BULLETIN VALUES('2020-03-26','après-midi','VERCORS',2,NULL,NULL);
INSERT INTO BULLETIN VALUES('2018-04-14','matin','COMPIEGNE',NULL,1,NULL);
INSERT INTO BULLETIN VALUES('2020-04-17','matin','ANJOU',NULL,NULL,2);
INSERT INTO POSSEDE_PREVISION VALUES(1,'2020-03-25','soirée');
INSERT INTO POSSEDE_PREVISION VALUES(2,'2018-04-14','matin');


CREATE OR REPLACE VIEW vAlerteMeteo(date_bulletin,periode_bulletin,lieu,type,vigilance,seuil,restrictions)
  AS 
 SELECT B.date_prevision,B.periode,B.lieu,a->>'type',a->>'vigilance',a->>'seuil',a->>'restrictions'
  FROM BULLETIN B,  JSON_ARRAY_ELEMENTS(B.alertes) a, Lieu L
   WHERE B.lieu = L.nom;

  CREATE OR REPLACE VIEW vHistoriqueCapteur(capteur,lieu,debut,fin)
  AS
  SELECT * FROM HISTORIQUE_CAPTEUR;
  
  CREATE OR REPLACE VIEW vLieuxConcernes(lieu,departement,region,date_prevision,precipitation,temperature,vent)
  AS
  SELECT B.lieu,D.numero,R.nom,B.date_prevision,P.type,NULL,NULL
  FROM LIEU L, DEPARTEMENT D, REGION R, PRECIPITATION P , CAPTEUR C, HISTORIQUE_CAPTEUR H, BULLETIN B
  WHERE D.region = R.nom
  AND D.numero = L.departement
  AND H.lieu = L.nom
  AND H.capteur = C.reference
  AND P.capteur=C.reference
  AND P.id = B.id_precipitation
  UNION
  SELECT B.lieu,D.numero,R.nom,B.date_prevision,' ',T.temp_reel,CAST (NULL as INT)
  FROM LIEU L, DEPARTEMENT D, REGION R, TEMPERATURE T, CAPTEUR C, HISTORIQUE_CAPTEUR H, BULLETIN B
  WHERE D.region = R.nom
  AND D.numero = L.departement
  AND H.lieu = L.nom
  AND H.capteur = C.reference
  AND T.capteur=C.reference
  AND T.id = B.id_temperature
  UNION
  SELECT B.lieu,D.numero,R.nom,B.date_prevision,'',NULL,V.force_vent
  FROM LIEU L, DEPARTEMENT D, REGION R, VENT V, CAPTEUR C, HISTORIQUE_CAPTEUR H, BULLETIN B
  WHERE D.region = R.nom
  AND D.numero = L.departement
  AND H.lieu = L.nom
  AND H.capteur = C.reference
  AND V.capteur=C.reference
  AND V.id = B.id_vent;
  
  CREATE OR REPLACE VIEW vNonCouverts AS
SELECT * FROM LIEU L WHERE L.nom NOT IN (SELECT L.nom FROM LIEU L,HISTORIQUE_CAPTEUR H WHERE L.nom=H.lieu);


  


  