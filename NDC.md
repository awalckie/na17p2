# Note de Clarification du projet 22

## Description du projet
L'objectif de ce projet est disposer d'une base de donnée permettant d'émettre des bulletins météo.


## Liste des objets nécessaires à la modélisation
**Bulletin**
- possède une date
- possède une période (matin, après-midi,soirée,nuit)
- comporte des prévisions
- peut comporter une prévision de type vent ou/et precipitation ou/et temperature
- est lié à un lieu


**Prevision**
- possède un identifiant (hypothèse)
- possède une description
- relevé par des capteurs


**Precipitation**
- possède un identifiant(hypothèse)
- possède une description
- possède un type (pluie, neige)
- relevé par des capteurs


**Vent**
- possède un identifiant(hypothèse)
- possède une description
- caractérisé par une force de vent et de rafales
- possède une direction(N,S,E,O)
- relevé par des capteurs


**Temperature**
- possède un identifiant(hypothèse)
- possède une description
- possède une température réelle
- possède une température ressentie
- relevé par des capteurs

**Capteur**
- possède une reference
- peut être hors-service ou fonctionnel

**Lieu**
- possède un nom (hypothèse unique)
- possède des capteurs

**Ville (hérite de Lieu)** 
- appartient à un département

**Montagne (hérite de Lieu)**
- se situe sur un à deux départements

**Departement**
- possède  un numéro
- est rattaché à une région

**Région**
- possède un nom
- peut être dans la métropole ou bien outre-mer

**HistoriqueCapteur**
- possède une référence
- possède un lieu
- possède une date de début d'affectation
- possède une date de fin d'affectation

**Alerte**
- possède un identifiant (hypothèse)
- possède un type
- possède un seuil 
- prevoit des restrictions

**Personne**
- possède un nom,prénom,email,telephone
- est responsable d'un lieu
## Contraintes

- Une force pour le vent sera nécéssairement positive
- Un capteur ne peut être affecté à deux lieux en même temps

## Utilisateurs de l'application


Membre de l'organisme français de météorologie


## Droits des utilisateurs

**Membre**
- Ajouter des lieux, des bulletins (avec leur date et leurs prévisions) et des capteurs.
- Supprimer les lieux (ville qui n'est plus "couverte" par exemple).
- Afficher les informations d'un bulletin pour une date et un lieu précis.
- Peut modifier l'etat d'un capteur

## Vues
- Une vue qui permet de calculer les taux moyens de différentes mesures (température, force du vent, etc.) pour un lieu, un département ou une région sur une période de l'année précise.
- Une vue qui permet de voir les lieux, départements ou régions les plus concernés par un type particulier de précipitation, une certaine température ou force de vent à une période de l'année.
- Une vue qui permet d'afficher l'historique de l'affectation des capteurs.
- Une vue qui détaille des alertes météo.


## Hypothèses faites pour la modélisation
    H0 : Les prévisions sont différentes des mesures de température,vent et précipitation
	H1 : Si la prévision provient d'un capteur hors-service, elle n'est pas prise en compte
    H2 : Une prévision autre que vent, température, precipitation pourra avoir une valeur en renseignant l'unité de mesure de celle-ci

