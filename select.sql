/*Afficher les informations d'un bulletin pour une date et un lieu précis*/
SELECT * FROM BULLETIN WHERE lieu='VERCORS' AND date_prevision < '2020-07-06';

/*Calculer les taux moyens de différentes mesures (température, force du vent, etc.) pour un lieu, un département ou une région sur une période de l'année précise.*/
SELECT AVG(V.temperature) FROM vLieuxConcernes V WHERE V.lieu='VERCORS' AND V.date_prevision < '2020-06-07';
SELECT AVG(V.vent) FROM vLieuxConcernes V WHERE V.departement=38 AND V.date_prevision < '2020-06-07';

SELECT * FROM vAlerteMeteo WHERE vigilance='rouge';

/* Voir les lieux, départements ou régions les plus concernés par un type particulier de précipitation, une certaine température ou force de vent à une période de l'année.*/
SELECT lieu,COUNT(lieu) AS nb FROM vLieuxConcernes WHERE temperature>10 GROUP BY lieu ORDER BY nb;
SELECT lieu,COUNT(lieu) AS nb FROM vLieuxConcernes WHERE vent<150 GROUP BY lieu ORDER BY nb;

