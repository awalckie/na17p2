#!/usr/bin/python3
import psycopg2
from os import system,name

# Connect to an existing database
class Meteo:
	def __init__(self):
		self.conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % ("tuxa.sme.utc", "dbbdd0p106", "bdd0p106", "BeEZrA1h"))
		self.cur = self.conn.cursor()
		self.menu()
		
	def __del__(self):
		self.conn.close()
		print("connexion closed")

	def menu(self):
		choice ='0'
		while choice<'1' or choice >'6':
			print("*************Menu Principal*************")
			print("1- Afficher une table")
			print("2- Afficher alerte meteo")
			print("3- Afficher previsions les plus fréquentes");
			print("4- Afficher taux moyen d'un prévision pour un lieu et une période");	
			print("5- Inserer un bulletin (avec alerte JSON)");
			print("6- Quitter");	
			choice = input()
			if choice =='1':
				self.menu_affichage()
			if choice =='2':
				self.vue_alertes()
			if choice =='3':
				self.param_lieux_concernes()
			if choice =='4':
				self.param_taux_moyens()
			if choice =='5':
				self.insert_bulletin()
			if choice =='6':
				break;
			else:
				continue

	def menu_affichage(self):
		choice_table ='0'
		while choice_table<'1' or choice_table >'12':
			print("*************Menu Tables*************")
			print("1- Bulletin")
			print("2- Capteur");
			print("3- Precipitation");	
			print("4- Temperature");
			print("5- Vent");		
			print("6- Previion_Simple");
			print("7- Personne")
			print("8- Responsable");
			print("9- Lieu");	
			print("10- Departement");
			print("11- Region");		
			print("12- Possede_Prevision");
			choice_table = input()
			if choice_table =='1':
				self.print_table("BULLETIN")
				break;
			if choice_table =='2':
				self.print_table("CAPTEUR")
				break;
			if choice_table =='3':
				self.print_table("PRECIPITATION")
				break;
			if choice_table =='4':
				self.print_table("TEMPERATURE")
				break;
			if choice_table =='5':
				self.print_table("VENT")
				break;
			if choice_table =='6':
				self.print_table("PREVISION_SIMPLE")
				break;
			if choice_table =='7':
				self.print_table("PERSONNE")
				break;
			if choice_table =='8':
				self.print_table("RESPONSABLE")
				break;
			if choice_table =='9':
				self.print_table("LIEU")
				break;
			if choice_table =='10':
				self.print_table("DEPARTEMENT")
				break;
			if choice_table =='11':
				self.print_table("REGION")
				break;
			if choice_table =='12':
				self.print_table("POSSEDE_PREVISION")
				break;
			else:
				continue
		self.menu()

			
	
	def vue_alertes(self):
		sql = "SELECT * FROM valertemeteo"
		self.cur.execute(sql)
		raw = self.cur.fetchone()
		print ("date_bulletin | periode_bulletin | lieu | type | vigilance | seuil | restrictions")
		while raw:
			print(" {} | {} | {} | {} | {} | {} | {} ".format(raw[0],raw[1],raw[2],raw[3],raw[4],raw[5],raw[6]))
			raw = self.cur.fetchone()
		self.menu()
	
	def param_lieux_concernes(self):
		choice ='0'
		while choice<'1' or choice >'3':
			print("informations sur :")
			print("1- Lieu")
			print("2- Departement")
			print("3- Region")
			choice = input()
			if choice =='1':
				info="lieu"
			if choice =='2':
				info="departement"
			if choice =='3':
				info="region"
			else:
				continue
		choice ='0'
		while choice<'1' or choice >'3':
			print("informations sur :")
			print("1- temperature")
			print("2- vent")
			print("3- precipitation")
			choice = input()
			if choice =='1':
				prevision="temperature"
			if choice =='2':
				prevision="vent"
			if choice =='3':
				prevision="precipitation"
				choice_precipitation='0'
				while choice_precipitation<'1' or choice_precipitation >'3':
					print("Quel type de precipitation?")
					print("1- neige")
					print("2- pluie")
					print("3- grêle")
					choice_precipitation = input()
					if choice_precipitation =='1':
						precipitation="'neige'"
					if choice_precipitation =='2':
						precipitation="'pluie'"
					if choice_precipitation =='3':
						precipitation="'grêle'"
					else:
						continue
				operateur="="
				self.vue_lieux_concernes(info,prevision,operateur,precipitation)
				break
			else:
				continue
		choice ='0'
		while choice<'1' or choice >'3':
			print("opérateur :")
			print("1- <")
			print("2- =")
			print("3- >")
			choice = input()
			if choice =='1':
				operateur="<"
			if choice =='2':
				operateur="="
			if choice =='3':
				operateur=">"
			else:
				continue
		print("Valeur?")
		valeur=input()
		self.vue_lieux_concernes(info,prevision,operateur,valeur)

		

		
	
	def vue_lieux_concernes(self,info,prevision,operateur,valeur):
		sql="SELECT {},Count({}) AS nb FROM vLieuxConcernes WHERE {}{}{} GROUP BY {} ORDER BY nb".format(info,info,prevision,operateur,valeur,info)
		self.cur.execute(sql)
		res = self.cur.fetchall()
		print ("lieu|nb")
		print(res)
		self.menu()
	
	def param_taux_moyens(self):
		choice ='0'
		while choice<'1' or choice >'3':
			print("Calculer une moyenne")
			print("1- temperature")
			print("2- vent")
			choice = input()
			if choice =='1':
				prevision="temperature"
			if choice =='2':
				prevision="vent"
		choice ='0'
		while choice<'1' or choice >'3':
			print("informations sur :")
			print("1- Lieu")
			print("2- Departement")
			print("3- Region")
			choice = input()
			if choice =='1':
				info="lieu"
				print("Quel est son nom")
			if choice =='2':
				info="departement"
				print("Quel est son numero")
			if choice =='3':
				info="region"
				print("Quel est son nom")
			else:
				continue
		
		name = input()
		name = self.to_sql(name)
		print("Date début de période : (format aaaa-mm-jj)")
		date_debut = input()
		date_debut = self.to_sql(date_debut)
		print("Date fin de période : (format aaaa-mm-jj)")
		date_fin = input()
		date_fin = self.to_sql(date_fin)
		self.vue_taux_moyens(info,prevision,name,date_debut,date_fin)
		
	def vue_taux_moyens(self,info,prevision,name,date_deb,date_fin):
		sql="SELECT AVG({}) AS moyenne FROM vLieuxConcernes WHERE {}={} AND date_prevision>{} AND date_prevision<{}".format(prevision,info,name,date_deb,date_fin)
		self.cur.execute(sql)
		res = self.cur.fetchall()
		title=info +"|nb"
		print(title)
		print(res)
		self.menu()
	
	def insert_bulletin(self):
		print("DATE format(aaaa-mm-jj")
		date = input()
		date = self.to_sql(date)
		print("periode:")
		choice='0'
		while choice<'1' or choice >'4':
			print("informations sur :")
			print("1- matin")
			print("2- après-midi")
			print("3- soirée")
			print("4- nuit")
			choice = input()
			if choice =='1':
				periode="'matin'"
			if choice =='2':
				periode="'après-midi'"
			if choice =='3':
				periode="'soirée'"
			if choice =='4':
				periode="'nuit'"
			else:
				continue
		
		print("Lieu parmi:")	
		self.liste_nom_lieux()
		lieu = input()
		lieu= self.to_sql(lieu)	
		temperature="0"
		while(temperature<"1" or temperature>"99" and temperature!="NULL"):
			print("Temperature : id ou NULL")
			self.liste_id_temperature()
			temperature =input()	
		precipitation="0"
		while(precipitation<"1" or precipitation>"99" and precipitation!="NULL"):
			print("Precipitation : id ou NULL")
			self.liste_id_precipitation()
			precipitation =input()
		vent="0"
		while(vent<"1" or vent>"99" and vent!="NULL"):
			print("vent : id ou NULL")
			self.liste_id_vent()
			vent =input()
		alertes="'["
		choice='o'
		first=1;
		while choice=='o':
			print("Ajouter une alerte? o/n")
			choice = input()
			if choice =='o':
				if first==1:
					alerte="{"
					first=0
				else :
					alerte=",{"
				print("type?")
				tmp =input()
				alerte = alerte + '"type":' + self.to_json_string(tmp) +','
				print("vigilance (rouge,jaune,verte)")
				tmp =input()
				alerte = alerte + '"vigilance":' + self.to_json_string(tmp) +','
				print("seuil?")
				tmp =input()
				alerte = alerte + '"seuil":' + tmp +','
				print("restrictions")
				tmp =input()
				alerte = alerte + '"restrictions":' +self.to_json_string(tmp) +'}'
				alertes=alertes+alerte
			if choice =='n':
				break
		alertes= alertes +"]'"
		try:
			sql = "INSERT INTO BULLETIN VALUES ({},{},{},{},{},{},{})".format(date,periode,lieu,temperature,precipitation,vent,alertes)
			print (sql)
			self.cur.execute(sql)
			self.conn.commit()
			self.menu()
		except psycopg2.IntegrityError as e:
			conn.rollback()
			print("Message système :", e)
			self.menu()
		
		
		
	

	def liste_nom_lieux(self):
		self.print_one_attribute("nom","lieu")

	def liste_id_precipitation(self):
		self.print_one_attribute("id","precipitation")

	def liste_id_temperature(self):
		self.print_one_attribute("id","temperature")

	def liste_id_vent(self):
		self.print_one_attribute("id","vent")
	
	def print_one_attribute(self,att,table):
		sql="SELECT {} from {}".format(att,table)
		self.cur.execute(sql)
		res = self.cur.fetchall()
		print(res)
	
	def print_table(self,table):
		sql="SELECT * from {}".format(table)
		self.cur.execute(sql)
		res = self.cur.fetchall()
		for i in res:
			print (i)
			print ('\n')

	
	def to_sql(self,s):
		return "'"+s+"'"

	def to_json_string(self,value):
		return '"'+value+'"'

		


met = Meteo()












